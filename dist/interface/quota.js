"use strict";

var deepmerge = require("deepmerge");

var responseHandlers = require("../response.js");
var fetch = require("../request.js").fetch;
var davTools = require("./dav.js");
var parseXML = require("./dav.js").parseXML;

var getValueForKey = davTools.getValueForKey;
var getSingleValue = davTools.getSingleValue;
var translateDiskSpace = davTools.translateDiskSpace;

function getQuota(options) {
    var fetchURL = options.remoteURL + "/";
    var fetchOptions = {
        method: "PROPFIND",
        headers: deepmerge({ Depth: 0 }, options.headers)
    };
    fetchURL = fetchURL.replace(/\/+$/g, "/");
    return fetch(fetchURL, fetchOptions).then(responseHandlers.handleResponseCode).then(function __convertToText(res) {
        return res.text();
    }).then(parseXML).then(parseQuota);
}

function parseQuota(result) {
    var responseItem = null,
        multistatus = void 0,
        propstat = void 0,
        props = void 0,
        quotaUsed = void 0,
        quotaAvail = void 0;
    try {
        multistatus = getValueForKey("multistatus", result);
        responseItem = getSingleValue(getValueForKey("response", multistatus));
    } catch (e) {
        /* ignore */
    }
    if (responseItem) {
        propstat = getSingleValue(getValueForKey("propstat", responseItem));
        props = getSingleValue(getValueForKey("prop", propstat));
        quotaUsed = getSingleValue(getValueForKey("quota-used-bytes", props));
        quotaAvail = getSingleValue(getValueForKey("quota-available-bytes", props));
        return typeof quotaUsed !== "undefined" && typeof quotaAvail !== "undefined" ? {
            used: parseInt(quotaUsed, 10),
            available: translateDiskSpace(quotaAvail)
        } : null;
    }
    return null;
}

module.exports = {
    getQuota: getQuota
};