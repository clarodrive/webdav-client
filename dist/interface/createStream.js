"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var Stream = require("stream");
var joinURL = require("url-join");
var deepmerge = require("deepmerge");

var fetch = require("../request.js").fetch;
var responseHandlers = require("../response.js");

var PassThroughStream = Stream.PassThrough;

function createReadStream(filePath, options) {
    var outStream = new PassThroughStream();
    getFileStream(filePath, options).then(function __handleStream(stream) {
        stream.pipe(outStream);
    }).catch(function __handleReadError(err) {
        outStream.emit("error", err);
    });
    return outStream;
}

function createWriteStream(filePath, options) {
    var writeStream = new PassThroughStream();
    var headers = deepmerge({}, options.headers);
    // if (typeof options.range === "object" && typeof options.range.start === "number") {
    //     var rangeHeader = "bytes=" + options.range.start + "-";
    //     if (typeof options.range.end === "number") {
    //         rangeHeader += options.range.end;
    //     }
    //     options.headers.Range = rangeHeader;
    // }
    if (options.overwrite === false) {
        headers["If-None-Match"] = "*";
    }
    var fetchURL = joinURL(options.remoteURL, filePath);
    var fetchOptions = {
        method: "PUT",
        headers: headers,
        body: writeStream
    };
    fetch(fetchURL, fetchOptions);
    return writeStream;
}

function getFileStream(filePath, options) {
    var rangeHeader = void 0;
    if (_typeof(options.range) === "object" && typeof options.range.start === "number") {
        rangeHeader = "bytes=" + options.range.start + "-";
        if (typeof options.range.end === "number") {
            rangeHeader += options.range.end;
        }
        options.headers.Range = rangeHeader;
    }
    var fetchURL = joinURL(options.remoteURL, filePath);
    var fetchOptions = {
        method: "GET",
        headers: options.headers
    };
    return fetch(fetchURL, fetchOptions).then(responseHandlers.handleResponseCode).then(function __mapResultToStream(res) {
        return res.body;
    });
}

module.exports = {
    createReadStream: createReadStream,
    createWriteStream: createWriteStream
};