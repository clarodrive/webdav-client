"use strict";

var joinURL = require("url-join");
var deepmerge = require("deepmerge");

var responseHandlers = require("../response.js");
var fetch = require("../request.js").fetch;

function moveFile(filename, destination, options) {
    var fetchURL = joinURL(options.remoteURL, filename);
    var fetchOptions = {
        method: "MOVE",
        headers: deepmerge({
            Destination: joinURL(options.remoteURL, destination)
        }, options.headers)
    };
    return fetch(fetchURL, fetchOptions).then(responseHandlers.handleResponseCode);
}

module.exports = {
    moveFile: moveFile
};