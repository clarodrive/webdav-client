"use strict";

var setFetchMethod = require("./request.js").setFetchMethod;
var createClient = require("./factory.js").createClient;

createClient.setFetchMethod = setFetchMethod;

module.exports = createClient;